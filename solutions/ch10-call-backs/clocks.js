function clock(id) {
    const indent = ('    ').repeat(id-1) + id + ')'
    return () => {
        console.log(indent, new Date().toLocaleTimeString());
    }
}

const c1 = setInterval(clock(1), 250)
const c2 = setInterval(clock(2), 1000)
const c3 = setInterval(clock(3), 3000)

setTimeout(() => {
    [c1,c2,c3].forEach(clk => clearInterval(clk))
}, 10*1000)

