
let date = new Date(process.argv[2] || '2022-01-24')

let weekdays = [
    'söndag', 'måndag', 'tisdag', 'onsdag', 'torsdag', 'fredag', 'lördag'
]

function weekdayOf(date) {
    return weekdays[date.getDay()]
}

console.log('%o är en %s', date, weekdayOf(date));

