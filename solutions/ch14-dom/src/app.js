
class Todo {
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(text, before = new Date(), done = false) {
        this.text = text
        this.done = done
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    toDOM() {
        const article = document.createElement('article')
        article.innerHTML = `
            <h5>
                <span>${this.before.toDateString()}</span>
                <button>Done</button>
                <button>Remove</button>
            </h5>
            <p>${this.text}</p>
        `
        if (this.done) {
            article.classList.add('done')
        }
        this.dom = article
        return article
    }

}

let tasks = []


function add(text, before, done = false) {
    tasks.push(new Todo(text, before, done))
    tasks.sort((lhs, rhs) => lhs.before.getTime() - rhs.before.getTime())
}

function populate() {
    const tasksEl = document.querySelector('#tasks')
    for (let task of tasks) {
        tasksEl.appendChild(task.toDOM())
    }
}


add('Understand CSS', '2022-01-25')
add('Learn JS basics', '2022-01-27', true)
add('Read about DOM model', '2022-01-26', false)
populate()
