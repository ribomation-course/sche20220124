
function reverseWords(sentence) {
    return sentence.split(' ').reverse().join(' ')
}


let input = process.argv[2] || 'JS is a cool language'
let output = reverseWords(input)
console.log('%o --> %o', input, output);
