function sum(n) {
    let result = 0;
    for (let k=1; k<=n; ++k) result += k;
    return result;
}

function prod(n) {
    let result = 1;
    for (let k=1; k<=n; ++k) result *= k;
    return result;
}

function table(n) {
    for (let k=1; k<=n; ++k) {
        console.log('%d\t%d\t%d', k, sum(k), prod(k));
    }
}

let N = +process.argv[2] || 10
console.log('sum(%d) = %d', N, sum(N));
console.log('prod(%d) = %d', N, prod(N));

table(N);
