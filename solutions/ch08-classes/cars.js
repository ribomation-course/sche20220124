let data = [{
    "id": 1,
    "producer": "Mazda",
    "model": "Miata MX-5",
    "year": 2002,
    "vin": "JTHKD5BH3D2268269",
    "city": "Tyukalinsk"
  }, {
    "id": 2,
    "producer": "Cadillac",
    "model": "Escalade EXT",
    "year": 2004,
    "vin": "1G6KE54Y14U271786",
    "city": "Tsuruga"
  }, {
    "id": 3,
    "producer": "Mercedes-Benz",
    "model": "E-Class",
    "year": 1992,
    "vin": "1GYFK56249R696309",
    "city": "Muheza"
  }, {
    "id": 4,
    "producer": "Chevrolet",
    "model": "Blazer",
    "year": 1997,
    "vin": "WBA3B1G51FN332178",
    "city": "Shuiyang"
  }, {
    "id": 5,
    "producer": "BMW",
    "model": "7 Series",
    "year": 1999,
    "vin": "YV126MEB9F1959273",
    "city": "Richky"
  }, {
    "id": 6,
    "producer": "Toyota",
    "model": "Sequoia",
    "year": 2002,
    "vin": "WAUPL68E75A629567",
    "city": "Santa Cecília"
  }, {
    "id": 7,
    "producer": "Acura",
    "model": "TL",
    "year": 2010,
    "vin": "WAUJFAFHXCN996952",
    "city": "Dungarvan"
  }, {
    "id": 8,
    "producer": "GMC",
    "model": "Savana 3500",
    "year": 2010,
    "vin": "WAUGL78E58A963848",
    "city": "Praia de Mira"
  }, {
    "id": 9,
    "producer": "Lexus",
    "model": "ES",
    "year": 1996,
    "vin": "WAUJC68E24A601387",
    "city": "Balzers"
  }, {
    "id": 10,
    "producer": "Porsche",
    "model": "Cayenne",
    "year": 2013,
    "vin": "5UXFE4C57AL401764",
    "city": "Áno Merá"
  }];


  class Car {
    constructor() {
        this.id = 0
        this.producer = ''
        this.model = ''
        this.year = 0
        this.vin = ''
        this.city = ''
    }

    static fromData(data) {
        return Object.assign(new Car(), data)
    }

  }

//console.log(Car.fromData(data[0]));

let cars = data.map(obj => Car.fromData(obj))

//

cars = cars.sort((lhs, rhs) => rhs.year - lhs.year)
console.log(cars.slice(0, 4));
