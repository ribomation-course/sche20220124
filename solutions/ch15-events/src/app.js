
class Todo {
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(text, before = new Date(), done = false) {
        this.text = text
        this.done = done
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    toDOM(index) {
        const article = document.createElement('article')
        article.innerHTML = `
            <h5>
                <span>${this.before.toDateString()}</span>
                <button class="done">Done</button>
                <button class="remove">Remove</button>
            </h5>
            <p>${this.text}</p>
        `
        this.dom = article
        if (this.done) {
            article.classList.add('done')
        }

        const self = this
        const doneBtn = article.querySelector('.done')
        doneBtn.addEventListener('click', ev => {
            self.toggle()
            if (self.done) {
                self.dom.classList.add('done')
            } else {
                self.dom.classList.remove('done')
            }
        })

        const removeBtn = article.querySelector('.remove')
        removeBtn.addEventListener('click', ev => {
            console.log('remove: %d', index);
            const evt = new CustomEvent('remove-todo', {detail: {index: index, dom: self.dom}})
            const tasksEl = document.querySelector('#tasks')
            tasksEl.dispatchEvent(evt)
        })
        
        return article
    }

}

let tasks = []


function add(text, before, done = false) {
    tasks.push(new Todo(text, before, done))
    tasks.sort((lhs, rhs) => lhs.before.getTime() - rhs.before.getTime())
}

function populate() {
    const tasksEl = document.querySelector('#tasks')
    tasksEl.innerHTML = ''
    let index = 0
    for (let task of tasks) {
        tasksEl.appendChild(task.toDOM(index++))
    }
}


add('Understand CSS', '2022-01-25')
add('Learn JS basics', '2022-01-27', true)
add('Read about DOM model', '2022-01-26', false)
populate()


const tasksEl = document.querySelector('#tasks')
tasksEl.addEventListener('remove-todo', ev => {
    console.log('remove-todo: %o', ev);

    const index =  ev.detail.index
    const todoDOM =  ev.detail.dom

    tasks.splice(index, 1)
    tasksEl.removeChild(todoDOM)
})

const saveBtn = document.querySelector('#saveBtn')
saveBtn.addEventListener('click', ev => {
    const textFld = document.querySelector('#textFld')
    const beforeFld = document.querySelector('#beforeFld')

    const textVal = textFld.value
    const beforeVal = beforeFld.value
    if (textVal && beforeVal) {
        add(textVal, beforeVal)
        populate()
        textFld.value = ''
        beforeFld.value = ''
    }

})
