
async function getData() {
    const jsonUrl = 'https://catalog.skl.se/rowstore/dataset/4e7a4004-9ebb-4ae7-8346-9f573aba3951/json'
    const res = await fetch(jsonUrl)
    const data = await res.json()
    return data.results
}

let regionData;
getData().then(data => {
    regionData = data
    console.log(data)
})

function lookup(regionName) {
    if (regionData) {
        return regionData.find(r => r.region.includes(regionName))
    } else {
        throw new Error('fetch data first')
    }
}

const map = document.querySelector('#sweden')

map.addEventListener("load", () => {
    const svg = map.getSVGDocument()
    const regions = svg.querySelectorAll('path')

    const name = document.querySelector('#region-name')
    const population = document.querySelector('#region-population')
    const code = document.querySelector('#region-code')

    const bg = '#eee'
    const bgActive = 'orange'
    const svgNS = "http://www.w3.org/2000/svg"

    regions.forEach(r => {
        const data = r.dataset

        r.style['stroke-width'] = 1 + 'px'
        r.style['stroke'] = 'blue'
        r.style['fill'] = bg

        const titleEl = document.createElementNS(svgNS, 'title')
        const txt = `Region ${r.getAttribute('title')}`
        titleEl.appendChild(document.createTextNode(txt))
        r.appendChild(titleEl)

        r.addEventListener('mouseover', ev => {
            const title = r.getAttribute('title')
            console.log(title);

            r.style.fill = bgActive

            const remoteData = lookup(title)
            if (remoteData) {
                console.log(remoteData)
                name.innerText = remoteData['region']
                code.innerText = remoteData['länskod']
                population.innerText = remoteData['folkmängd 31 december 2020']
            }
        })

        r.addEventListener('mouseout', ev => {
            r.style.fill = bg
            name.innerText = ''
            code.innerText = ''
            population.innerText = ''
        })

        r.addEventListener('click', ev => {
            const title = r.getAttribute('title')
            console.log(title);
            const url = `https://sv.wikipedia.org/wiki/Region_${title}`
            const opts = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes"
            window.open(url, "Region " + title, opts);
        })
    })

})