function css(sel) {
    return document.querySelector(sel)
}

function updateStatus(ev) {
    if (navigator.onLine) {
        css('#status span').innerText = 'connected'
        css('#imgCon').classList.remove('hidden')
        css('#imgDis').classList.add('hidden')
    } else {
        css('#status span').innerText = 'disconnected'
        css('#imgCon').classList.add('hidden')
        css('#imgDis').classList.remove('hidden')
    }

    const netInf = navigator.connection
    console.log('net info: %o', netInf);
    css('#type').innerText = netInf.effectiveType
    css('#speed').innerText = netInf.downlink
    css('#time').innerText = netInf.rtt
    css('#reduced').innerText = netInf.saveData
}

window.onoffline = updateStatus
window.ononline = updateStatus
updateStatus()

