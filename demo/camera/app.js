let video
let videoTrack
let zoom

document.querySelector('#start').addEventListener('click', async ev => { start() })
document.querySelector('#capture').addEventListener('click', async ev => { capture() })
document.querySelector('#stop').addEventListener('click', async ev => { stop() })


async function start() {
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        const msg = document.querySelector('#message')
        msg.innerHTML = 'media capture not supported'
        throw new Error('media capture not supported')
    }

    try {
        const stream = await navigator.mediaDevices.getUserMedia({ video: {zoom: true} })
        const [track] = stream.getVideoTracks()

        const constraints = {
            aspectRatio: {min: 1, ideal: 1},
            width: {min: 100, ideal: 450},
            height: {min: 100, ideal: 450},
        }
        await track.applyConstraints(constraints)

        // https://googlechrome.github.io/samples/image-capture/update-camera-zoom.html
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamTrack/applyConstraints

        const settings = track.getSettings();
        if ('zoom' in settings) {
            const zoom = document.querySelector('#zoom')

            const cap = track.getCapabilities()
            zoom.min = cap.zoom.min
            zoom.max = cap.zoom.max
            zoom.step = cap.zoom.step
            zoom.value = settings.zoom

            zoom.addEventListener('input', ev => {
                const zoomValue = ev.target.value
                this.videoTrack.applyConstraints({
                    advanced: [ {zoom: zoomValue} ]
                })
            })
        } else {
            console.error('zoom not supported')
            console.warn('settings: %o', settings)
            console.warn('capabilities: %o', track.getCapabilities())
        }

        video = document.querySelector('#video')
        video.srcObject = stream
        videoTrack = track
        document.querySelector('#camera').innerHTML = track.label
    } catch (err) {
        console.error('** failed: %o', err)
    }
}

async function stop() {
    if (video.srcObject) {
        video.srcObject.getTracks().forEach(t => t.stop())
    }
    video.srcObject = null
}

async function capture() {
    const blob = await new ImageCapture(track).takePhoto()
    const photo = document.querySelector('#photo')
    photo.src = URL.createObjectURL(blob)

    const label = document.querySelector('#label')
    label.innerHTML = new Date().toLocaleString()

    const card = document.querySelector('.card')
    card.classList.remove('hidden')
}
