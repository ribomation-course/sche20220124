console.log('Using the == operator');
console.log('1 == 1: %o', 1 == 1);
console.log('1 == "1": %o', 1 == "1");
console.log('0 == false: %o', 0 == false);
console.log('0 == null: %o', 0 == null);
console.log('null == undefined: %o', null == undefined);

console.log('Using the === operator');
console.log('1 === 1: %o', 1 === 1);
console.log('1 === "1": %o', 1 === "1");
console.log('0 === false: %o', 0 === false);
console.log('0 === null: %o', 0 === null);
console.log('null === undefined: %o', null === undefined);

