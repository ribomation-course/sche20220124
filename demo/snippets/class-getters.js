class Product {
    constructor(name, price, count) {
        this._name = name
        this._price = price
        this._count = count
    }
    static VAT = 25
    get name() { return this._name.toUpperCase() }
    get price() { return this._price * (1 + Product.VAT / 100) }
    get count() { return this._count }
    get outOfStock() { return this._count === 0 }

    set price(amount) {
        if (amount <= 0) {
            throw new Error('price amount must be positive. was ' + amount)
        }
        this._price = amount
    }
    set count(amount) {
        if (amount <= 0) {
            throw new Error('stock count amount must be positive. was ' + amount)
        }
        this._count = amount
    }

    toString() {
        const opts  = {minimumFractionDigits:2, maximumFractionDigits:2, useGrouping:true}
        const stock = this.outOfStock ? 'out of stock' : `${this.count} item${this.count===1 ? '' : 's'} in stock`
        return `Product{${this.name}, ${this.price.toLocaleString('sv', opts)} kr, ${stock}}`
    }
}

let p = new Product('Coco Nut', 2.25, 0)
console.log(p);
console.log(p.toString());

let q = new Product('JS Primer', 1100, 5)
console.log(q.toString());

q.price = 1000
q.count = 1
console.log(q.toString());

try {
    q.price = -10
} catch (err) {
    console.log('*** %o', err.message);
}


