function average(...numbers) {
    if (numbers.length === 0)  return 0
    return numbers.reduce((sum,n) => sum+n, 0) / numbers.length
}

console.log('[1] %d', average(0,5,10,15,20,25,30))
console.log('[2] %d', average(5,5,5,5,555))
console.log('[3] %d', average())
console.log('[4] %d', average(10,20,30,-500))

